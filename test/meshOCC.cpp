#include <cstddef>
#include <cstdio>
#include <cmath>
#include <string>
#include <iostream>
#include <fstream>
#define OCCGEOMETRY
namespace nglib {
#include "nglib.h"

}
#undef OCCGEOMETRY

char * out_file = "outfile.stl";

void meshOCC(char* fileName, float size);

using namespace std;

void WriteSTLFormat (nglib::Ng_Mesh * mesh,
         const string & filename)
{
  cout << "\nWrite STL Surface Mesh" << endl;

  ostream *outfile;

  outfile = new ofstream(filename.c_str());

  int i;

  outfile->precision(10);

  *outfile << "solid" << endl;

  for (i = 1; i <= nglib::Ng_GetNSE(mesh); i++)
    {
      *outfile << "facet normal ";
      int pi[3];
      nglib::Ng_GetSurfaceElement (mesh, i, pi);
      double A[3], B[3], C[3];
      nglib::Ng_GetPoint (mesh, pi[0], A);
      nglib::Ng_GetPoint (mesh, pi[1], B);
      nglib::Ng_GetPoint (mesh, pi[2], C);

      double ba[3], ca[3], norm[3];
      for (int j=0; j<3; j++)
      {
        ba[j] = B[j]-A[j];
        ca[j] = C[j]-A[j];
      }
      norm[0] = ba[1]*ca[2]-ba[2]*ca[1];
      norm[1] = ba[2]*ca[0]-ba[0]*ca[2];
      norm[2] = ba[0]*ca[1]-ba[1]*ca[0];

      double len = sqrt(norm[0]*norm[0]+norm[1]*norm[1]+norm[2]*norm[2]);

      if (len != 0)
      {
        for (int j=0; j<3; j++)
          norm[j] /= len;
      }

      *outfile << norm[0] << " " << norm[1] << " " << norm[2]<< "\n";
      *outfile << "outer loop\n";

      *outfile << "vertex " << A[0] << " " << A[1] << " " << A[2] << "\n";
      *outfile << "vertex " << B[0] << " " << B[1] << " " << B[2] << "\n";
      *outfile << "vertex " << C[0] << " " << C[1] << " " << C[2] << "\n";

      *outfile << "endloop\n";
      *outfile << "endfacet\n";
    }
  *outfile << "endsolid" << endl;
}

int main (int argc, char** argv)
{
  if (argc < 2 || argc > 4)
  {
    printf("Usage: meshOCC <occGeomFile> [<sizing> <outfile> ]\n");
    return 0;
  }

  float size = 0.1;
  if (argc >= 3)
  {
    sscanf(argv[2], "%g", &size);
  }

  if (argc >= 4)
    out_file = argv[3];

  meshOCC(argv[1], size) ;
  return 0;
}

void meshOCC(char* fileName, float size) {

  nglib::Ng_Init();

  nglib::Ng_OCC_Geometry* occGeom = nglib::Ng_OCC_Load_STEP(fileName);
  printf("Finished reading geometry\n");
  nglib::Ng_Meshing_Parameters* meshParams = new nglib::Ng_Meshing_Parameters();
  meshParams->uselocalh = 0;
  meshParams->maxh = size;
  meshParams->optsurfmeshenable = 0;

  nglib::Ng_Result result;

  nglib::Ng_Mesh* occNetGenMesh = nglib::Ng_NewMesh();
  printf("Allocated mesh\n");

  result = nglib::Ng_OCC_SetLocalMeshSize(occGeom, occNetGenMesh, meshParams);
  printf("OCC Set Local Mesh Size Result: %d\n", result);
  result =
      nglib::Ng_OCC_GenerateEdgeMesh(occGeom, occNetGenMesh, meshParams);
  printf("OCC Generate Edge Mesh Result: %d\n", result);
  int numPnts = nglib::Ng_GetNP(occNetGenMesh);
  printf("OCC Number of Points: %d\n", numPnts);

  result =
    nglib::Ng_OCC_GenerateSurfaceMesh(occGeom, occNetGenMesh, meshParams);

  printf("OCC Result: %d\n", result);
  printf("OCC Number of Points: %d\n", nglib::Ng_GetNP(occNetGenMesh));
  printf("OCC Number of Surface Elements: %d\n",
    nglib::Ng_GetNSE(occNetGenMesh));

  // write an stl file
  if (out_file)
  {
    string ofile(out_file);
    WriteSTLFormat(occNetGenMesh, ofile);
  }

  nglib::Ng_DeleteMesh(occNetGenMesh);
  nglib::Ng_OCC_DeleteGeometry(occGeom);
  delete meshParams;

  nglib::Ng_Exit();
}
