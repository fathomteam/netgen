#include <cstddef>
#include <cstdio>
#include <cmath>

#define OCCGEOMETRY
namespace nglib {
#include "nglib.h"
}
#undef OCCGEOMETRY

void testSTL();
void testOCC();
void testOCCWithAddPoint();

int main ()
{
  testSTL();
  testOCC();
  testOCCWithAddPoint();

  return 0;
}

void testSTL()
{
  nglib::Ng_Init();

  nglib::Ng_STL_Geometry* stlGeom = nglib::Ng_STL_LoadGeometry("brick.stl");
  nglib::Ng_STL_InitSTLGeometry(stlGeom);
  nglib::Ng_Meshing_Parameters* meshParams = new nglib::Ng_Meshing_Parameters();
  meshParams->maxh = 0.1;

  nglib::Ng_Mesh* netGenMesh = nglib::Ng_NewMesh();

  nglib::Ng_Result result =
    nglib::Ng_STL_MakeEdges(stlGeom, netGenMesh, meshParams);
  printf("Make Edges Result: %d\n", result);

  result =
    nglib::Ng_STL_GenerateSurfaceMesh(stlGeom, netGenMesh, meshParams);

  printf("Result: %d\n", result);
  printf("Number of Points: %d\n", nglib::Ng_GetNP(netGenMesh));
  printf("Number of Surface Elements: %d\n", nglib::Ng_GetNSE(netGenMesh));

  int lmnt[3];
  nglib::Ng_GetSurfaceElement(netGenMesh, 1, lmnt);
  printf("First Element: %d %d %d\n", lmnt[0], lmnt[1], lmnt[2]);

  nglib::Ng_DeleteMesh(netGenMesh);
  delete stlGeom;
  delete meshParams;

  nglib::Ng_Exit();
}

void testOCC() {

  nglib::Ng_Init();

  nglib::Ng_OCC_Geometry* occGeom = nglib::Ng_OCC_Load_STEP("brick.stp");
  printf("Finished reading geometry\n");
  nglib::Ng_Meshing_Parameters* meshParams = new nglib::Ng_Meshing_Parameters();
  meshParams->maxh = 0.1;

  nglib::Ng_Result result;

  //nglib::Ng_OCC_TopTools_IndexedMapOfShape* fmapPtr =
    //new nglib::Ng_OCC_TopTools_IndexedMapOfShape();
  //nglib::Ng_Result result = nglib::Ng_OCC_GetFMap(occGeom, fmapPtr);

  //printf("OCC Result: %d\n", result);

  nglib::Ng_Mesh* occNetGenMesh = nglib::Ng_NewMesh();
  printf("Allocated mesh\n");

  result =
    nglib::Ng_OCC_GenerateEdgeMesh(occGeom, occNetGenMesh, meshParams);
  printf("OCC Generate Edge Mesh Result: %d\n", result);
  int numPnts = nglib::Ng_GetNP(occNetGenMesh);
  printf("OCC Number of Points: %d\n", numPnts);
  double pnt[3];
  for (int i = 0; i < numPnts; ++i)
  {
    nglib::Ng_GetPoint(occNetGenMesh, i + 1, pnt);
    printf("Point %d: %f %f %f\n", i + 1, pnt[0], pnt[1], pnt[2]);
  }

  result =
    nglib::Ng_OCC_GenerateSurfaceMesh(occGeom, occNetGenMesh, meshParams);

  printf("OCC Result: %d\n", result);
  printf("OCC Number of Points: %d\n", nglib::Ng_GetNP(occNetGenMesh));
  printf("OCC Number of Surface Elements: %d\n",
    nglib::Ng_GetNSE(occNetGenMesh));

  int lmnt[3];
  nglib::Ng_GetSurfaceElement(occNetGenMesh, 1, lmnt);
  printf("OCC First Element: %d %d %d\n", lmnt[0], lmnt[1], lmnt[2]);

  nglib::Ng_DeleteMesh(occNetGenMesh);
  nglib::Ng_OCC_DeleteGeometry(occGeom);
  delete meshParams;

  nglib::Ng_Exit();
}

void testOCCWithAddPoint() {

  nglib::Ng_Init();

  nglib::Ng_OCC_Geometry* occGeom = nglib::Ng_OCC_Load_STEP("brick.stp");
//  nglib::Ng_OCC_Geometry* occGeom = nglib::Ng_OCC_Load_STEP("testGeom/holycyl.stp");
  printf("Finished reading geometry\n");
  nglib::Ng_Meshing_Parameters* meshParams = new nglib::Ng_Meshing_Parameters();
//  meshParams->maxh = 0.03125;
/*
  meshParams->maxh = 1.5;
  meshParams->fineness = 0.875;
  meshParams->grading = 0.125;
  meshParams->elementsperedge = 6;
  meshParams->elementspercurve = 6;
  meshParams->closeedgeenable = 1;
  meshParams->closeedgefact = 10.0;
*/
  meshParams->uselocalh = 0;
  meshParams->optsurfmeshenable = 1;
  meshParams->maxh = 0.25;
//  meshParams->elementsperedge = 1;
//  meshParams->elementspercurve = 1;
//  meshParams->closeedgeenable = 0;
//  meshParams->closeedgefact = 0.0;

  nglib::Ng_Result result;

  //nglib::Ng_OCC_TopTools_IndexedMapOfShape* fmapPtr =
    //new nglib::Ng_OCC_TopTools_IndexedMapOfShape();
  //nglib::Ng_Result result = nglib::Ng_OCC_GetFMap(occGeom, fmapPtr);

  //printf("OCC Result: %d\n", result);

  nglib::Ng_Mesh* occNetGenMesh = nglib::Ng_NewMesh();
  printf("Allocated mesh\n");

  nglib::Ng_OCC_SetLocalMeshSize(occGeom, occNetGenMesh, meshParams);

  double pnt[3];
  pnt[0] = 0.5;
  pnt[1] = 0.5;
  pnt[2] = 0.375;
  nglib::Ng_AddPoint(occNetGenMesh, pnt);
  printf("Added a point\n");

  result =
    nglib::Ng_OCC_GenerateEdgeMesh(occGeom, occNetGenMesh, meshParams);
  printf("OCC Generate Edge Mesh Result: %d\n", result);
  int numPnts = nglib::Ng_GetNP(occNetGenMesh);
  printf("OCC Number of Points: %d\n", numPnts);
  for (int i = 0; i < numPnts; ++i)
  {
    nglib::Ng_GetPoint(occNetGenMesh, i + 1, pnt);
    printf("Point %d: %f %f %f\n", i + 1, pnt[0], pnt[1], pnt[2]);
  }

  pnt[0] = 0.5;
  pnt[1] = 0.5;
  pnt[2] = 0.125;
  nglib::Ng_AddPoint(occNetGenMesh, pnt);
  printf("Added another point\n");

  result =
    nglib::Ng_OCC_GenerateSurfaceMesh(occGeom, occNetGenMesh, meshParams);

  printf("OCC Result: %d\n", result);
  numPnts = nglib::Ng_GetNP(occNetGenMesh);
  printf("OCC Number of Points: %d\n", numPnts);
  for (int i = 0; i < numPnts; ++i)
  {
    nglib::Ng_GetPoint(occNetGenMesh, i + 1, pnt);
    int bdryCnt = 0;
    if (0.5 - fabs(pnt[0]) < 0.001) ++bdryCnt;
    if (0.5 - fabs(pnt[1]) < 0.001) ++bdryCnt;
    if (0.5 - fabs(pnt[2]) < 0.001) ++bdryCnt;
    if (bdryCnt >= 1)
    {
      printf("Point %d: %f %f %f\n", i + 1, pnt[0], pnt[1], pnt[2]);
    }
  }
  int numElmnts = nglib::Ng_GetNSE(occNetGenMesh);
  printf("OCC Number of Surface Elements: %d\n", numElmnts);

  for (int i = 0; i < numElmnts; ++i)
  {
    int lmnt[3];
    nglib::Ng_GetSurfaceElement(occNetGenMesh, i + 1, lmnt);
    if (lmnt[0] <= 10 || lmnt[1] <= 10 || lmnt[2] <= 10)
    {
      printf("OCC Element %d: %d %d %d\n", i + 1, lmnt[0], lmnt[1], lmnt[2]);
    }
  }

  nglib::Ng_DeleteMesh(occNetGenMesh);
  nglib::Ng_OCC_DeleteGeometry(occGeom);
  delete meshParams;

  nglib::Ng_Exit();
}
